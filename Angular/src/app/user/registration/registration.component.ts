import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styles: [
  ]
})
export class RegistrationComponent implements OnInit {

  constructor(public service: UserService) { }

  ngOnInit(): void {
    this.service.formModel.reset();
  }

  onSubmit(){
    this.service.registration().subscribe(
      (res) => {
        console.log(res);
        if(res == null)
          this.service.formModel.reset();
      },
      (err:any) => {   
        if(err.ok)
          this.service.formModel.reset();
        else {        
          switch(err.error){
            case 'DuplicateUserName' :
              console.log('DuplicateUserName');
            break;
            case 'InvalidUserName' :
              console.log('InvalidUserName');
            break;
            default:
              console.log(err);
            break;
          }
        }
      }
    );
  }
}
