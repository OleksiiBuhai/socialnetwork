import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private fb:FormBuilder, private http:HttpClient) { }

  readonly BaseURI = "http://localhost:65309/api/";

  formModel = this.fb.group({
    UserName : ['', Validators.required],
    Email : ['', Validators.email],
    Passwords : this.fb.group({
      Password : ['', [Validators.required, Validators.minLength(6)]],
      ConfirmPassword : ['', Validators.required]
    }, { validator : this.comparePasswords }),    
    FirstName : [''],
    LastName : [''],
    Birthday : ['']
  });

  comparePasswords(fb:FormGroup){
    let confirmPass = fb.get('ConfirmPassword');
    if(confirmPass?.errors == null || 'passwordMismatch' in confirmPass.errors){
      if(fb.get('Password')?.value != confirmPass?.value)
        confirmPass?.setErrors({ passwordMismatch : true });
      else
        confirmPass?.setErrors(null);
    }
  }

  registration(){
    let body = {
      UserName: this.formModel.value.UserName,
      Email: this.formModel.value.Email,
      FirstName: this.formModel.value.FirstName,
      LastName: this.formModel.value.LastName,
      Password: this.formModel.value.Passwords.Password,
      Birthday: this.formModel.value.Birthday
    };
    return this.http.post(this.BaseURI+"user/registration", body);
  }

  login(formData:any) {
    return this.http.post(this.BaseURI+"user/login", formData);
  }

  getUserProfile(){
    return this.http.get(this.BaseURI+'UserProfile');
  }

  roleMatch(allowedRoles:any): boolean {
    var isMatch = false;
    var token = localStorage.getItem('token') ?? "";
    var payLoad = JSON.parse(window.atob(token.split('.')[1]));
    var userRole = payLoad.role;
    allowedRoles.forEach((element:any) => {
      if (userRole == element) {
        isMatch = true;
      }
    });
    return isMatch;
  }
}
