﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Validation
{
    public class SocialNetworkException : Exception
    {
        public SocialNetworkException() : base() { }

        public SocialNetworkException(string message) : base(message)
        {
        }

        public SocialNetworkException(string message, Exception inner)
        : base(message, inner)
        {
        }
    }
}
