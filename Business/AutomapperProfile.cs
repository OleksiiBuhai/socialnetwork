﻿using AutoMapper;
using Business.Dto;
using Data.Entities;
using System.Linq;

namespace Business
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Chat, ChatDto>()
                .ForMember(c => c.MembersIds, m => m.MapFrom(member => member.Members.Select(x => x.Id)))
                .ForMember(c => c.MessagesIds, m => m.MapFrom(message => message.Messages.Select(x => x.Id)))
                .ReverseMap();

            CreateMap<Message, MessageDto>()
                .ForMember(a => a.AuthorId, m => m.MapFrom(message => message.Author.Id))
                .ReverseMap();

            CreateMap<ApplicationUser, UserDto>()
                .ForMember(u => u.Birthday, p => p.MapFrom(profile => profile.UserProfile.Birthday))
                .ForMember(u => u.FirstName, p => p.MapFrom(profile => profile.UserProfile.FirstName))
                .ForMember(u => u.LastName, p => p.MapFrom(profile => profile.UserProfile.LastName))
                .ReverseMap();
                //.ForMember(u => u.UserProfile, p => p.MapFrom(prop => new UserProfile { FirstName = prop.FirstName, LastName = prop.LastName, Birthday = prop.Birthday }));
        }
    }
}
