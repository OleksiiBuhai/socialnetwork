﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Dto
{
    public class ChatDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<string> MembersIds { get; set; }
        public virtual ICollection<string> MessagesIds { get; set; }
    }
}
