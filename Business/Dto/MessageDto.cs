﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Dto
{
    public class MessageDto
    {
        public string Id { get; set; }
        public string Text { get; set; }

        public string ChatId { get; set; }
        public string AuthorId { get; set; }
    }
}
