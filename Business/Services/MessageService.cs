﻿using AutoMapper;
using Business.Dto;
using Business.Interfaces;
using Data.Entities;
using Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class MessageService : IMessageService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;

        public MessageService(IUnitOfWork unitOfWork, IMapper mapper, UserManager<ApplicationUser> userManager)
        {
            _uow = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task AddAsync(MessageDto model)
        {
            var msg = _mapper.Map<MessageDto, Message>(model);
            msg.Author = await _userManager.FindByIdAsync(msg.AuthorId);
            msg.Chat = await _uow.ChatRepository.GetByIdAsync(msg.ChatId);
            await _uow.MessageRepository.AddAsync(msg);
            await _uow.SaveAsync();
            model.Id = msg.Id;
        }

        public async Task DeleteByIdAsync(string modelId)
        {
            await _uow.MessageRepository.DeleteByIdAsync(modelId);
            await _uow.SaveAsync();
        }

        public IEnumerable<MessageDto> GetAll()
        {
            return _mapper.Map<IQueryable<Message>, IEnumerable<MessageDto>>(_uow.MessageRepository.FindAll());
        }

        public async Task<MessageDto> GetByIdAsync(string id)
        {
            return _mapper.Map<Message, MessageDto>(await _uow.MessageRepository.GetByIdAsync(id));
        }

        public async Task<IEnumerable<MessageDto>> GetMessagesByChatId(string chatId)
        {
            return await Task.Run(() => GetAll().Where(m => m.ChatId == chatId));
        }

        public async Task UpdateAsync(MessageDto model)
        {
            var msg = _mapper.Map<MessageDto, Message>(model);
            _uow.MessageRepository.Update(msg);
            await _uow.SaveAsync();
        }
    }
}
