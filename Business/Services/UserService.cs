﻿using AutoMapper;
using Business.Dto;
using Business.Interfaces;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _uow = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task AddAsync(UserDto model)
        {
            var user = new ApplicationUser { Email = model.Email, UserName = model.UserName };
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                var profile = new UserProfile { Id = user.Id, FirstName = model.FirstName, LastName = model.LastName, Birthday = model.Birthday };
                await _uow.UserProfileRepository.AddAsync(profile);
                await _userManager.AddToRoleAsync(user, model.Role);
                await _uow.SaveAsync();
                model.Id = user.Id;
            }
            else
            {
                throw new SocialNetworkException(result.Errors.FirstOrDefault().Code);
            }
        }

        public async Task DeleteByIdAsync(string modelId)
        {
            var user = await _userManager.FindByIdAsync(modelId);
            await _uow.UserProfileRepository.DeleteByIdAsync(user.UserProfile.Id);
            await _userManager.DeleteAsync(user); 
        }

        public IEnumerable<UserDto> GetAll()
        {
            return _mapper.Map<IQueryable<ApplicationUser>, IEnumerable<UserDto>>(_userManager.Users);
        }

        public async Task<UserDto> GetByIdAsync(string id)
        {
            return _mapper.Map<ApplicationUser, UserDto>(await _userManager.FindByIdAsync(id));
        }

        public async Task UpdateAsync(UserDto model)
        {
            var user = await _userManager.FindByIdAsync(model.Id);
            user.Email = model.Email;
            user.UserName = model.UserName;
            user.UserProfile.FirstName = model.FirstName;
            user.UserProfile.LastName = model.LastName;
            user.UserProfile.Birthday = model.Birthday;
            await _userManager.UpdateAsync(user);
        }
    }
}
