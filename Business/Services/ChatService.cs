﻿using AutoMapper;
using Business.Dto;
using Business.Interfaces;
using Data.Entities;
using Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class ChatService : IChatService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;

        public ChatService(IUnitOfWork unitOfWork, IMapper mapper, UserManager<ApplicationUser> userManager)
        {
            _uow = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task AddAsync(ChatDto model)
        {
            var chat = _mapper.Map<ChatDto, Chat>(model);
            var list = new List<ApplicationUser>();
            foreach (var id in model.MembersIds)
                list.Add(await _userManager.FindByIdAsync(id));
            chat.Members = list;
            await _uow.ChatRepository.AddAsync(chat);
            await _uow.SaveAsync();
            model.Id = chat.Id;
        }

        public async Task DeleteByIdAsync(string modelId)
        {
            await _uow.ChatRepository.DeleteByIdAsync(modelId);
            await _uow.SaveAsync();
        }

        public IEnumerable<ChatDto> GetAll()
        {
            return _mapper.Map<IQueryable<Chat>, IEnumerable<ChatDto>>(_uow.ChatRepository.FindAll());
        }

        public async Task<ChatDto> GetByIdAsync(string id)
        {
            return _mapper.Map<Chat, ChatDto>(await _uow.ChatRepository.GetByIdAsync(id));
        }

        public async Task UpdateAsync(ChatDto model)
        {
            var chat = _mapper.Map<ChatDto, Chat>(model);
            _uow.ChatRepository.Update(chat);
            await _uow.SaveAsync();
        }
    }
}
