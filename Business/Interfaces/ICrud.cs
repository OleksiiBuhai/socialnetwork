﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface ICrud<TDto> where TDto : class
    {
        IEnumerable<TDto> GetAll();

        Task<TDto> GetByIdAsync(string id);

        Task AddAsync(TDto model);

        Task UpdateAsync(TDto model);

        Task DeleteByIdAsync(string modelId);
    }
}
