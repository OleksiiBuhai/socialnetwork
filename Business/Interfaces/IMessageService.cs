﻿using Business.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IMessageService : ICrud<MessageDto>
    {
        Task<IEnumerable<MessageDto>> GetMessagesByChatId(string chatId);
    }
}
