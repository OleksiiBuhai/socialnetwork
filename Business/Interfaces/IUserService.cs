﻿using Business.Dto;

namespace Business.Interfaces
{
    public interface IUserService : ICrud<UserDto>
    {
    }
}
