﻿using Data.Interfaces;
using Data.Repositories;
using System.Threading.Tasks;

namespace Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SocialNetworkContext _db;
        private UserProfileRepository _userProfileRepository;
        private ChatRepository _chatRepository;
        private MessageRepository _messageRepository;
        public UnitOfWork(SocialNetworkContext db)
        {
            _db = db;
        }

        public IUserProfileRepository UserProfileRepository
        {
            get
            {
                if (_userProfileRepository is null)
                    _userProfileRepository = new UserProfileRepository(_db);
                return _userProfileRepository;
            }
        }

        public IChatRepository ChatRepository
        {
            get
            {
                if (_chatRepository is null)
                    _chatRepository = new ChatRepository(_db);
                return _chatRepository;
            }
        }

        public IMessageRepository MessageRepository
        {
            get
            {
                if (_messageRepository is null)
                    _messageRepository = new MessageRepository(_db);
                return _messageRepository;
            }
        }

        public Task<int> SaveAsync()
        {
            return _db.SaveChangesAsync();
        }
    }
}
