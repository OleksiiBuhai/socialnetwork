﻿using Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class SocialNetworkContext : IdentityDbContext
    {
        public SocialNetworkContext(DbContextOptions options) : base (options)
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Message> Messages { get; set; }
    }
}
