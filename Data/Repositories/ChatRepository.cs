﻿using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ChatRepository : IChatRepository
    {
        private readonly SocialNetworkContext _db;

        public ChatRepository(SocialNetworkContext db)
        {
            _db = db;
        }

        public async Task AddAsync(Chat entity)
        {
            await _db.Chats.AddAsync(entity);
        }

        public void Delete(Chat entity)
        {
            _db.Remove(entity);
        }

        public async Task DeleteByIdAsync(string id)
        {
            var entity = await GetByIdAsync(id);

            if (entity != null)
            {
                _db.Chats.Remove(entity);
            }
        }

        public IQueryable<Chat> FindAll()
        {
            return _db.Chats;
        }

        public async Task<Chat> GetByIdAsync(string id)
        {
            return await _db.Chats.FindAsync(id);
        }

        public void Update(Chat entity)
        {
            _db.Chats.Update(entity);
        }
    }
}
