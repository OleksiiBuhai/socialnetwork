﻿using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class MessageRepository : IMessageRepository
    {
        private readonly SocialNetworkContext _db;

        public MessageRepository(SocialNetworkContext db)
        {
            _db = db;
        }

        public async Task AddAsync(Message entity)
        {
            await _db.Messages.AddAsync(entity);
        }

        public void Delete(Message entity)
        {
            _db.Remove(entity);
        }

        public async Task DeleteByIdAsync(string id)
        {
            var entity = await GetByIdAsync(id);

            if (entity != null)
            {
                _db.Messages.Remove(entity);
            }
        }

        public IQueryable<Message> FindAll()
        {
            return _db.Messages;
        }

        public async Task<Message> GetByIdAsync(string id)
        {
            return await _db.Messages.FindAsync(id);
        }

        public void Update(Message entity)
        {
            _db.Messages.Update(entity);
        }
    }
}
