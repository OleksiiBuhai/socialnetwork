﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;

using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly SocialNetworkContext _db;

        public UserProfileRepository(SocialNetworkContext db)
        {
            _db = db;
        }

        public async Task AddAsync(UserProfile entity)
        {
            await _db.UserProfiles.AddAsync(entity);
        }

        public void Delete(UserProfile entity)
        {
            _db.Remove(entity);
        }

        public async Task DeleteByIdAsync(string id)
        {
            var entity = await GetByIdAsync(id);

            if (entity != null)
            {
                _db.UserProfiles.Remove(entity);
            }
        }

        public IQueryable<UserProfile> FindAll()
        {
            return _db.UserProfiles;
        }

        public async Task<UserProfile> GetByIdAsync(string id)
        {
            return await _db.UserProfiles.FindAsync(id);
        }

        public void Update(UserProfile entity)
        {
            _db.UserProfiles.Update(entity);
        }
    }
}
