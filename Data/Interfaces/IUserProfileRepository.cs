﻿using Data.Entities;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IUserProfileRepository : IRepository<UserProfile>
    {
    }
}
