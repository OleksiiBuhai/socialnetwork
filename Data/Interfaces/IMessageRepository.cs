﻿using Data.Entities;

namespace Data.Interfaces
{
    public interface IMessageRepository : IRepository<Message>
    {
    }
}
