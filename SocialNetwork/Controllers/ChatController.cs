﻿using Business.Dto;
using Business.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Controllers
{
    [Route("api/[controller]")]    
    [ApiController]
    public class ChatController : ControllerBase
    {
        private readonly IChatService _chatService;
        private readonly IMessageService _messageService;
        public ChatController(IChatService chatService, IMessageService messageService)
        {
            _chatService = chatService;
            _messageService = messageService;
        }

        [HttpPost]
        [Route("NewChat")]
        //POST : /api/Chat/NewChat
        public async Task<object> Add([FromBody] ChatDto chat)
        {
            try
            {
                await _chatService.AddAsync(chat);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return CreatedAtAction(nameof(Add), new { chat.Id }, chat);
        }

        [HttpPost]
        [Route("NewMessage")]
        //POST : /api/Chat/NewChat
        public async Task<object> SentMsg([FromBody] MessageDto msg)
        {
            try
            {
                await _messageService.AddAsync(msg);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return CreatedAtAction(nameof(Add), new { msg.Id }, msg);
        }

        //GET: /api/Chat/1
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<MessageDto>>> GetMessagesByChatId(string id)
        {
            var result = await _messageService.GetMessagesByChatId(id);
            return Ok(result);
        }

        //PUT: /api/Chat/
        [HttpPut]
        public async Task<ActionResult> Update(MessageDto model)
        {
            try
            {
                await _messageService.UpdateAsync(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        //DELETE: /api/Chat/1
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteChat(string id)
        {
            await _chatService.DeleteByIdAsync(id);
            return Ok();
        }

        //DELETE: /api/Chat/msg/1
        [HttpDelete("msg/{id}")]
        public async Task<ActionResult> DeleteMsg(string id)
        {
            await _messageService.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
