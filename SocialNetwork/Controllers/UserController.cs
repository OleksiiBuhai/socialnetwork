﻿using Business.Dto;
using Business.Interfaces;
using Data.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Models;

namespace SocialNetwork.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUserService _service;
        private readonly ApplicationSettings _applicationSettings;
        public UserController(IUserService userService, UserManager<ApplicationUser> userManager,IOptions<ApplicationSettings> applicationSettings)
        {
            _service = userService;
            _userManager = userManager;
            _applicationSettings = applicationSettings.Value;
        }

        [HttpPost]
        [Route("Registration")]
        //POST : /api/User/Registration
        public async Task<object> Add([FromBody] UserDto user)
        {
            user.Role = "RegisteredUser";
            try
            {
                await _service.AddAsync(user);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            //return CreatedAtAction(nameof(Add), new { user.Id }, user);
            return Ok();
        }

        //GET: /api/User/1
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<UserDto>>> GetById(string id)
        {
            var result = await _service.GetByIdAsync(id);
            return Ok(result);
        }

        //PUT: /api/User/
        [HttpPut]
        public async Task<ActionResult> Update(UserDto model)
        {
            try
            {
                await _service.UpdateAsync(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        //DELETE: /api/User/1
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            await _service.DeleteByIdAsync(id);
            return Ok();
        }

        [HttpPost]
        [Route("Login")]
        //POST : /api/ApplicationUser/Login
        public async Task<IActionResult> Login(LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                var role = await _userManager.GetRolesAsync(user);
                IdentityOptions _options = new IdentityOptions();
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserID", user.Id.ToString()),
                        new Claim(_options.ClaimsIdentity.RoleClaimType, role.FirstOrDefault())
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_applicationSettings.JWT_Secret)), SecurityAlgorithms.HmacSha256Signature)
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(securityToken);                
                return Ok(new { token });
            }
            else
                return BadRequest(new { message = "Username or password is incorrect." });
        }
    }
}
